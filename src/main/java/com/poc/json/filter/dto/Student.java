package com.poc.json.filter.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Builder @AllArgsConstructor @NoArgsConstructor
public class Student {
    private String id;
    private String name;
    private Address address;

    public Student(String name, String id) {
        this.id = id;
        this.name = name;
    }
}
