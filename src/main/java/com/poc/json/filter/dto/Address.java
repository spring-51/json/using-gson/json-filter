package com.poc.json.filter.dto;

import lombok.*;

@Data @Builder @AllArgsConstructor @NoArgsConstructor
public class Address {
    private String id;
    private String street;
    private String houseNumber;
    private FlatDetails flat;

    public Address(String id, String street, String houseNumber) {
        this.id = id;
        this.street = street;
        this.houseNumber = houseNumber;
    }
}
