package com.poc.json.filter;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.poc.json.filter.dto.Address;
import com.poc.json.filter.dto.FlatDetails;
import com.poc.json.filter.dto.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.sound.midi.Soundbank;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@SpringBootApplication
public class JsonFilterApplication {

	public static void main(String[] args) {
		// SpringApplication.run(JsonFilterApplication.class, args);
		Map<?, ?> filterdJson = filterSingleJsonObject();
		System.out.println("=========================================");
		System.out.println("FILTERED JSON RESPONSE");
		System.out.println(filterdJson);
		System.out.println("=========================================");
	}

	private static Map<?,?> filterSingleJsonObject() {
		Student student = getStudent(1);
		Gson gson = new Gson();
		String stringToParse = gson.toJson(student);

		JsonParser parser = new JsonParser();
		JsonObject json = (JsonObject) parser.parse(stringToParse);
		System.out.println("=========================================");
		System.out.println("Original POJO");
		System.out.println(json);
		System.out.println("=========================================");

		List<String> allowedKeys = List.of("name", "address#id", "address#flat#name");
		System.out.println("=========================================");
		System.out.println("Filter Query Params");
		System.out.println(allowedKeys);
		System.out.println("=========================================");

		Set<Map.Entry<String, JsonElement>> entries = json.entrySet();
		Map<String, String> responseFilteredJson = new HashMap<>();
		for(String allowedKey : allowedKeys){
			String[] splitKeys = allowedKey.split("#");
			JsonObject tempJson = json.deepCopy();
			if(splitKeys.length ==1){
				String value = entries.stream()
						.filter(e -> e.getKey().equalsIgnoreCase(allowedKey))
						.map(e -> e.getValue()).findFirst().get().getAsString();
				responseFilteredJson.put(allowedKey, value);
				// System.out.println(allowedKey+" ====== "+value);
			}else{
				int index=0;
				JsonElement jsonElement = null;
				do{
					jsonElement = tempJson.get(splitKeys[index++]);
					tempJson = jsonElement.getAsJsonObject();
				}while(index < splitKeys.length -1 );
				String asString = tempJson.get(splitKeys[index]).getAsString();
				responseFilteredJson.put(allowedKey, asString);
				// System.out.println(allowedKey+"======"+asString);
			}
		}
		// System.out.println(responseFilteredJson.toString());
		return responseFilteredJson;
	}

	private static Student getStudent(Integer index) {
		Student student = new Student("studentame" + index, "studentid" + index);
		Address address = new Address("addressid" + index, "saddresstreet" + index, "addresshouse" + index);
		address.setFlat(FlatDetails.builder().name("addressflatname"+index).id("addressflatid"+index).build());
		student.setAddress(address);
		return student;
	}


}
